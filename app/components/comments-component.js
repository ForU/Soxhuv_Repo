import Ember from 'ember';

export default Ember.Component.extend({
	didInsertElement: function()
	{
		 var disqus_loaded;
		/** Disqus Code Section **/
		$(document.body).on('click', '.show-comments', function(){
			  var disqus_shortname = 'alexmaglonso'; // Replace this value with *your* username.

			  // ajax request to load the disqus javascript
			if (Ember.typeOf(disqus_loaded) == "undefined" || !disqus_loaded) {
				  disqus_loaded = 1;
				  $.ajax({
						  type: "GET",
						  url: "//" + disqus_shortname + ".disqus.com/embed.js",
						  dataType: "script",
						  cache: true
				  });
			}
			else
			{
				$(this).html('Hide Comments');
				$("#disqus_thread").show();
				$(this).removeClass('show-comments').addClass('hide-comments');
			}
			$(this).html('Hide Comments');
			$("#disqus_thread").show();
			$(this).removeClass('show-comments').addClass('hide-comments');
		});
		
		$(document.body).on('click', '.hide-comments', function(){

			  // $("#disqus_thread").empty();
			  // // hide the button once comments load
			  // // $(this).fadeOut();
			  $(this).html('Show Comments');
			  $(this).removeClass('hide-comments').addClass('show-comments');
			  $("#disqus_thread").hide();
		});
		/** !Disqus Code Section **/
	}
});
